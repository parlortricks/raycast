;; title:   raycaster
;; author:  parlortricks
;; desc:    raycaster
;; license: GPL-3.0-or-later
;; SPDX-License-Identifier: GPL-3.0-or-later
;; version: 0.1
;; script:  fennel
;; strict:  true
;; saveid: parlortricks-raycaster-1
;; Copyright (c) 2022 parlortricks

;; Game specific data
(local GAME {:screen {:width 240
                      :height 136
                      :hwidth nil
                      :hheight nil}
             :raycasting {:incangle nil
                          :precision 24}})

;; Player specific data
(local player {:fov 60
               :hfov nil
               :x 2.5
               :y 2.5
               :angle 45
               :speed {:movement 0.2
                       :rotation 3.0}})

;; Level data
(local level [[1 1 1 1 1 1 1 1 1 1]
              [1 0 0 0 0 0 0 0 0 1]
              [1 0 0 0 0 0 0 0 0 1]
              [1 0 0 2 2 0 2 0 0 1]
              [1 0 0 2 0 0 2 0 0 1]
              [1 0 0 2 0 0 2 0 0 1]
              [1 0 0 2 0 2 2 0 0 1]
              [1 0 0 0 0 0 0 0 0 1]
              [1 0 0 0 0 0 0 0 0 1]
              [1 1 1 1 1 1 1 1 1 1]])
                                     
(fn deg-to-rad [degree]
  (/ (* degree math.pi) 180))	
  
(fn raycasting [] 
  (var ray-angle (- player.angle player.hfov))
  
  (for [ray-count 0 (- GAME.screen.width 1)]
    ;; ray data
    (local ray {:x player.x
                :y player.y})

    ;; ray path incrementers                            
    (local ray-cos (/ (math.cos (deg-to-rad ray-angle)) GAME.raycasting.precision))
    (local ray-sin (/ (math.sin (deg-to-rad ray-angle)) GAME.raycasting.precision))
    
    ;; wall finder
    (var wall 0)
    (while (= wall 0)
      (set ray.x (+ ray.x ray-cos))
      (set ray.y (+ ray.y ray-sin))			
      (set wall (. (. level (math.floor ray.y)) (math.floor ray.x))))
           
    ;; pythagoras theorum
    (var distance (math.sqrt (+ (math.pow (- player.x ray.x) 2) 
                                (math.pow (- player.y ray.y) 2))))

    ;; Fish eye fix
    (set distance (* distance (math.cos (deg-to-rad (- ray-angle player.angle)))))
    
    ;; wall height
    (var wall-height (math.floor (/ GAME.screen.hheight distance)))
    
    ;;draw
    (line ray-count 0 ray-count (- GAME.screen.hheight wall-height) 11) ;; ceiling
    (var wall-color 0)
    (if (= wall 1) (set wall-color 2) (set wall-color 15))
    (line ray-count (- GAME.screen.hheight wall-height) ray-count (+ GAME.screen.hheight wall-height) wall-color) ;;wall
    (line ray-count (+ GAME.screen.hheight wall-height) ray-count GAME.screen.height 5) ;; floor
    (set ray-angle (+ ray-angle GAME.raycasting.incangle))))


(fn input [] 
  (when (btn 0) ;;up
    (var player-cos (* (math.cos (deg-to-rad player.angle)) player.speed.movement))	
    (var player-sin (* (math.sin (deg-to-rad player.angle)) player.speed.movement))	

    ;; collision test
    (let [new-x (+ player.x player-cos)
          new-y (+ player.y player-sin)]
      (when (= (. (. level (math.floor new-y)) (math.floor new-x)) 0)
        (set player.x new-x)
        (set player.y new-y)))) 

  (when (btn 1) ;;down
    (var player-cos (* (math.cos (deg-to-rad player.angle)) player.speed.movement))	
    (var player-sin (* (math.sin (deg-to-rad player.angle)) player.speed.movement))	

    ;; collision test
    (let [new-x (- player.x player-cos)
          new-y (- player.y player-sin)]
      (when (= (. (. level (math.floor new-y)) (math.floor new-x)) 0)
        (set player.x new-x)
        (set player.y new-y)))) 

  (when (btn 2) ;;left
    (set player.angle (- player.angle player.speed.rotation))) 

  (when (btn 3) ;;right
    (set player.angle (+ player.angle player.speed.rotation))))

(fn update [] 
  (input))
  
(fn draw [] 
  (cls)
  (raycasting))

(fn _G.BOOT []
 (set GAME.screen.hwidth (/ GAME.screen.width 2))
 (set GAME.screen.hheight (/ GAME.screen.height 2))
 (set GAME.raycasting.incangle (/ player.fov GAME.screen.width))
 (set player.hfov (/ player.fov 2)))

(fn _G.TIC []
  (update)
  (draw))



;; <TILES>
;; 001:eccccccccc888888caaaaaaaca888888cacccccccacc0ccccacc0ccccacc0ccc
;; 002:ccccceee8888cceeaaaa0cee888a0ceeccca0ccc0cca0c0c0cca0c0c0cca0c0c
;; 003:eccccccccc888888caaaaaaaca888888cacccccccacccccccacc0ccccacc0ccc
;; 004:ccccceee8888cceeaaaa0cee888a0ceeccca0cccccca0c0c0cca0c0c0cca0c0c
;; 017:cacccccccaaaaaaacaaacaaacaaaaccccaaaaaaac8888888cc000cccecccccec
;; 018:ccca00ccaaaa0ccecaaa0ceeaaaa0ceeaaaa0cee8888ccee000cceeecccceeee
;; 019:cacccccccaaaaaaacaaacaaacaaaaccccaaaaaaac8888888cc000cccecccccec
;; 020:ccca00ccaaaa0ccecaaa0ceeaaaa0ceeaaaa0cee8888ccee000cceeecccceeee
;; </TILES>

;; <TRACKS>
;; 000:100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; </TRACKS>

;; <PALETTE>
;; 000:1a1c2c5d275db13e53ef7d57ffcd75a7f07038b76425717929366f3b5dc941a6f673eff7f4f4f494b0c2566c86333c57
;; </PALETTE>