;; title:   raycaster
;; author:  parlortricks
;; desc:    raycaster
;; license: GPL-3.0-or-later
;; SPDX-License-Identifier: GPL-3.0-or-later
;; version: 0.1
;; script:  fennel
;; strict:  true
;; saveid: parlortricks-raycaster-1
;; Copyright (c) 2022 parlortricks

;; Game specific data
(local GAME {:screen {:width 240
                      :height 136
                      :hwidth nil
                      :hheight nil
                      :scale 1
                      :translate 0.5}

             :projection {:width nil
                          :height nil
                          :hwidth nil
                          :hheight nil}

             :raycasting {:incangle nil
                          :precision 64}})

;; Player specific data
(local player {:fov 60
               :hfov nil
               :x 2.5
               :y 2.5
               :angle 45
               :speed {:movement 0.2
                       :rotation 3.0}})

;; Level data
(local level [[1 1 1 1 1 1 1 1 1 1]
              [1 0 0 0 0 0 0 0 0 1]
              [1 0 0 0 0 0 0 0 0 1]
              [1 0 0 2 2 0 2 0 0 1]
              [1 0 0 2 0 0 2 0 0 1]
              [1 0 0 2 0 0 2 0 0 1]
              [1 0 0 2 4 2 2 0 0 1]
              [1 0 0 0 0 0 0 0 0 1]
              [1 0 0 0 0 0 0 0 0 1]
              [1 1 1 1 1 1 1 1 1 1]])

(local textures [{:width 16
                  :height 16
                  :sprite 0}
                  
                {:width 16
                  :height 16
                  :sprite 2}

                {:width 16
                  :height 16
                  :sprite 4}                  

                {:width 32
                  :height 32
                  :sprite 4}  
                  ])

(fn vline [x1 y1 x2 y2 col]
  (line (+ (* x1 GAME.screen.scale) GAME.screen.translate)
        (+ (* y1 GAME.screen.scale) GAME.screen.translate)
        (+ (* x2 GAME.screen.scale) GAME.screen.translate)
        (+ (* y2 GAME.screen.scale) GAME.screen.translate)
        col))
                                     
(fn deg-to-rad [degree]
  (/ (* degree math.pi) 180))	

(fn sget [x y]
 "get sprite sheet pixel"
  (let [addr (+ 16384 (* (+ (// x 8) (* (// y 8) 16)) 32))]
    (peek4 (+ (+ (* addr 2) (% x 8)) (* (% y 8) 8)))))

(fn draw-texture [x wall-height texture-pos-x texture]
  (var y-inc (/ (* wall-height 2) texture.height))
  (var y (- GAME.projection.hheight wall-height))

  (for [i 0 texture.height]
    ;(local color (. texture.colors (. (. texture.bitmap i) (+ texture-pos-x 1))))
    (local frx (* (% texture.sprite 16) 8))
    (local fry (* (math.floor (/ texture.sprite 16)) 8))
    (local color (sget (+ frx texture-pos-x) (+ fry i)))

    (vline x y x (+ y y-inc 0.25) color)
    (set y (+ y y-inc))))
  
(fn raycasting [] 
  (var ray-angle (- player.angle player.hfov))
  
  (for [ray-count 0 (- GAME.projection.width 1)]
    ;; ray data
    (local ray {:x player.x
                :y player.y})

    ;; ray path incrementers                            
    (local ray-cos (/ (math.cos (deg-to-rad ray-angle)) GAME.raycasting.precision))
    (local ray-sin (/ (math.sin (deg-to-rad ray-angle)) GAME.raycasting.precision))
    
    ;; wall finder
    (var wall 0)
    (while (= wall 0)
      (set ray.x (+ ray.x ray-cos))
      (set ray.y (+ ray.y ray-sin))			
      (set wall (. (. level (math.floor ray.y)) (math.floor ray.x))))
           
    ;; pythagoras theorum
    (var distance (math.sqrt (+ (math.pow (- player.x ray.x) 2) 
                                (math.pow (- player.y ray.y) 2))))

    ;; Fish eye fix
    (set distance (* distance (math.cos (deg-to-rad (- ray-angle player.angle)))))
    
    ;; wall height
    (var wall-height (math.floor (/ GAME.projection.hheight distance)))

    ;; get texture
    (local texture (. textures wall))

    ;; calc texture position
    (local texture-position-x (+ (* (// texture.sprite texture.width) texture.width) (math.floor (% (* texture.width (+ ray.x ray.y)) texture.width)))	)

    ;;draw
    (vline ray-count 0 ray-count (- GAME.projection.hheight wall-height) 11) ;; ceiling
    ;(vline ray-count (- GAME.projection.hheight wall-height) ray-count (+ GAME.projection.hheight wall-height) 2) ;;wall
    (draw-texture ray-count wall-height texture-position-x texture)
    (vline ray-count (+ GAME.projection.hheight wall-height) ray-count GAME.projection.height 5) ;; floor
    (set ray-angle (+ ray-angle GAME.raycasting.incangle))))


(fn input [] 
  (when (btn 0) ;;up
    (var player-cos (* (math.cos (deg-to-rad player.angle)) player.speed.movement))	
    (var player-sin (* (math.sin (deg-to-rad player.angle)) player.speed.movement))	

    ;; collision test
    (let [new-x (+ player.x player-cos)
          new-y (+ player.y player-sin)]
      (when (= (. (. level (math.floor new-y)) (math.floor new-x)) 0)
        (set player.x new-x)
        (set player.y new-y)))) 

  (when (btn 1) ;;down
    (var player-cos (* (math.cos (deg-to-rad player.angle)) player.speed.movement))	
    (var player-sin (* (math.sin (deg-to-rad player.angle)) player.speed.movement))	

    ;; collision test
    (let [new-x (- player.x player-cos)
          new-y (- player.y player-sin)]
      (when (= (. (. level (math.floor new-y)) (math.floor new-x)) 0)
        (set player.x new-x)
        (set player.y new-y)))) 

  (when (btn 2) ;;left
    (set player.angle (- player.angle player.speed.rotation))) 

  (when (btn 3) ;;right
    (set player.angle (+ player.angle player.speed.rotation)))

  (when (btnp 4) ;; Z
    (set GAME.raycasting.precision (- GAME.raycasting.precision 2))
    (when (< GAME.raycasting.precision 2) (set GAME.raycasting.precision 2)))

  (when (btnp 5) ;; Z
    (set GAME.raycasting.precision (+ GAME.raycasting.precision 2))
    (when (> GAME.raycasting.precision 256) (set GAME.raycasting.precision 256)))
  )

(fn update [] 
  (input))
  
(fn draw [] 
  (cls)
  (raycasting)
  (print (.. :precision= GAME.raycasting.precision))
  )

(fn _G.BOOT []
 (set GAME.screen.hwidth (/ GAME.screen.width 2))
 (set GAME.screen.hheight (/ GAME.screen.height 2))
 (set GAME.raycasting.incangle (/ player.fov GAME.screen.width))
 (set player.hfov (/ player.fov 2)))

 (set GAME.projection.width (/ GAME.screen.width GAME.screen.scale))
 (set GAME.projection.height (/ GAME.screen.height GAME.screen.scale))
 (set GAME.projection.hwidth (/ GAME.projection.width 2))
 (set GAME.projection.hheight (/ GAME.projection.height 2))
 (set GAME.raycasting.incangle (/ player.fov GAME.projection.width))

(fn _G.TIC []
  (update)
  (draw))



;; <TILES>
;; 000:eeeeeeeeddddddddddddddddcddcddcdddcddcdddcddcdddcddcdddddddddddd
;; 001:eeeeeeeeddddedddddddeddddeddededddddedddddddeddddeddededddddeddd
;; 002:eeeeeeeeeeeeeeeeeeeeeeee00000000ccccc0cceeeee0eeeeeee0eeeeeee0ee
;; 003:eee0eeeeeee0eeeeeee0eeee00000000cccccccceeeeeeeeeeeeeeeeeeeeeeee
;; 004:eeeeeeeeebbabbbaebbabbbaebbaaaaaeabcbbbbebbcbbbbebbcbbbbebbcbbbb
;; 005:eeeeeeeebbbabbbabbbabbbaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
;; 006:eeeeeeeebbbabbbabbbabbbaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
;; 007:eeeeeeeebbbabbbabbbabbbaaaaaabbabbbbaaaabbbba9babbbba9babbbba9ba
;; 016:ddddddddeeeeeeeeccccecccdddcedddeddcededdddcedddeddcededdddceddd
;; 017:ddddedddeeeeeeeeccccccccddddddddddcdddcddcdddcddcdddcddddddddddd
;; 018:eeeee0ee00000000cccccccceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000
;; 019:eeeeeeee00000000cccccc0ceeeeee0eeeeeee0eeeeeee0eeeeeee0e00000000
;; 020:eabcbbbbebbcbbbbebbcbbbbebbcbbbbeabcbbbbebbcbbbbebbcbbbbebbcb4bb
;; 021:bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbdddddbbbdddddbbb
;; 022:bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
;; 023:bbbbaaaabbbba9babbbba9babbbba9babbbbaaaabbbba9babbbba9babbbba9ba
;; 036:eabcb4bbebbcb4bbebbcb4bbebbcbebbeabcbfbbebbcbbbbebbcbbbbebbcbbbb
;; 037:dddddbbbd4fddbbbe4eddbbbfeeddbbbdddddbbbfddddbbbbbbbbbbbbbbbbbbb
;; 038:bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
;; 039:bbbbaaaabbbba9babbbba9babbbba9babbbbaaaabbbba9babbbba9babbbba9ba
;; 052:eabcbbbbebbcbbbbebbcbbbbebbcbbbbeabcbbbbebbcccccebbabbbaebbabbba
;; 053:bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccbbbabbbabbbabbba
;; 054:bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccbbbabbbabbbabbba
;; 055:bbbbaaaabbbba9babbbba9babbbba9babbbba9baccccaaaabbbabbbabbbabbba
;; </TILES>

;; <SPRITES>
;; 001:eccccccccc888888caaaaaaaca888888cacccccccacc0ccccacc0ccccacc0ccc
;; 002:ccccceee8888cceeaaaa0cee888a0ceeccca0ccc0cca0c0c0cca0c0c0cca0c0c
;; 003:eccccccccc888888caaaaaaaca888888cacccccccacccccccacc0ccccacc0ccc
;; 004:ccccceee8888cceeaaaa0cee888a0ceeccca0cccccca0c0c0cca0c0c0cca0c0c
;; 017:cacccccccaaaaaaacaaacaaacaaaaccccaaaaaaac8888888cc000cccecccccec
;; 018:ccca00ccaaaa0ccecaaa0ceeaaaa0ceeaaaa0cee8888ccee000cceeecccceeee
;; 019:cacccccccaaaaaaacaaacaaacaaaaccccaaaaaaac8888888cc000cccecccccec
;; 020:ccca00ccaaaa0ccecaaa0ceeaaaa0ceeaaaa0cee8888ccee000cceeecccceeee
;; </SPRITES>

;; <TRACKS>
;; 000:100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; </TRACKS>

;; <PALETTE>
;; 000:1a1c2c5d275db13e53ef7d57ffcd75a7f07038b76425717929366f3b5dc941a6f673eff7f4f4f494b0c2566c86333c57
;; </PALETTE>

